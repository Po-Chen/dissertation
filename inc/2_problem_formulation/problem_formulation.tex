\chapter{Problem Formulation}
\label{ch:prob_formul}

Given a target object $\Ot$,
represented by either a plane or a dense surface model (\ie triangle mesh),
and an observed camera image $\Ic$,
the task of object pose recovering is to determine the object pose of $\Ot$
in 6DoF parameterization based on the orientation and
position of the object with respect to a calibrated camera.
%
With a set of reference points
$\mathbf{x}_{i} = \left[x_{i}, y_{i}, z_{i}\right]^\top, i = 1, \ldots, n, n\geq3$
in the coordinate system of $\Ot$,
and a set of camera-image coordinates
$\mathbf{u}_{i} = \left[u_{i}, v_{i}\right]^\top$
in $\Ic$ depicted by~\figref{coor_sys},
the transformation between them can be formulated as:
\begin{equation}
\label{eq:in_ex}
\begin{bmatrix}
hu_{i}\\
hv_{i}\\
h\\
\end{bmatrix}
=
\mathbf{K}
\begin{bmatrix}
\mathbf{R} | \mathbf{t}
\end{bmatrix}
\begin{bmatrix}
x_{i}\\
y_{i}\\
z_{i}\\
1
\end{bmatrix},
\end{equation}
where
\begin{equation}
\label{eq:K_R_and_t}
\mathbf{K}
=
\begin{bmatrix}
f_x & 0 & x_0 \\
0 & f_y & y_0 \\
0 & 0 & 1
\end{bmatrix},\
\mathbf{R} =
\begin{bmatrix}
R_{11} & R_{12} & R_{13} \\
R_{21} & R_{22} & R_{23} \\
R_{31} & R_{32} & R_{33}
\end{bmatrix}
\in \emph{SO}(3),\ 
\mathbf{t} =
\begin{bmatrix}
t_{x}\\
t_{y}\\
t_{z}
\end{bmatrix}
\in \mathbb{R}^{3},
\end{equation}
are the intrinsic matrix of the camera, rotation matrix, and translation vector, respectively.
%
In~\eqnref{in_ex},
$h$ is the scale factor representing the depth value in the camera coordinate system.
In~\eqnref{K_R_and_t}, $(f_{x}, f_{y})$ and $(x_{0}, y_{0})$ are the focal
length and the principal point of the camera, respectively.

\begin{figure}
\begin{center}
\includegraphics[width=0.815\linewidth]{inc/2_problem_formulation/figure/coor_sys/coor_sys.pdf}
\end{center}
\caption{
The perspective projection model.
$(O,\vec{i}_{o},\vec{j}_{o},\vec{k}_{o})$ is the object coordinate system,
$(C,\vec{i}_{c},\vec{j}_{c},\vec{k}_{c})$ is the camera coordinate system,
$\mathbf{x}_{i}$ is a 3D point,
and $\mathbf{u}_{i}$ is its projection onto the image plane.
}
\label{fig:coor_sys}
\end{figure}

Given the observed camera-image points $\hat{\mathbf{u}}_{i} =
\left[\hat{u}_{i},\hat{v}_{i}\right]^\top$, a pose estimation algorithm needs
to determine values a for pose $\mathbf{p} \equiv (\mathbf{R}, \mathbf{t})$
that minimize an appropriate error function.
The rotation of the pose $\mathbf{p}$ can be parameterized in 
numerous ways~\cite{grassia1998practical},
and will be further discussed in~\secref{param_rot}.
%including Euler angles (see~\secref{ape}) and axis-angle representation (see~\secref{pose_refinement}).

There are two types of error functions commonly used for pose estimation. 
%
The first one is called \textit{reprojection error} and is broadly used in the P$n$P algorithms:
\begin{equation}
\label{eq:E_re_Rt}
E_{r}(\mathbf{p}) = \frac{1}{n}\sum_{i = 1}^{n}
\left(
(\hat{u}_{i}-{u}_{i})^{2}
+
(\hat{v}_{i}-{v}_{i})^{2}
\right).
\end{equation}
%
The second type of error function is based on \textit{appearance distance}
and is primarily used in direct methods:
\begin{equation}
\label{eq:E_sad_Rt}
E_{a_{1}}(\mathbf{p})
=
\frac{1}{n}\sum_{i = 1}^{n}
|\Ic(\mathbf{u}_{i})-\Ot({\mathbf{x}}_{i})|,
\end{equation}
or
\begin{equation}
\label{eq:E_ssd_Rt}
E_{a_{2}}(\mathbf{p})
=
\frac{1}{n}\sum_{i = 1}^{n}
\left(\Ic(\mathbf{u}_{i})-\Ot({\mathbf{x}}_{i})\right)^{2},
\end{equation}
where $\Ic(\mathbf{u}_{i})$ is the image pixel value of $\Ic$ at $\mathbf{u}_{i}$,
and $\Ot({\mathbf{x}}_{i})$ is the texture pixel value of $\Ot$ at  ${\mathbf{x}}_{i}$.
In most cases, the pixel values are normalized in the range $[0,1]$.
The error functions in~\eqnref{E_sad_Rt} and~\eqnref{E_ssd_Rt} are
the normalized Sum-of-Absolute-Differences (SAD) and Sum-of-Squared-Difference (SSD) errors, respectively.
%

\section{Parameterization of Rotation}
\label{sec:param_rot}

The general form of a rotation in $\mathbb{R}^{3}$ is a
3$\times$3 orthogonal matrix with determinant 1.
However, the matrix representation with nine elements seems redundant 
since it only has a maximum of three degrees of freedom (3DoF).
Actually, there are many ways to parameterize 3DoF rotations with fewer parameters, such as
\textit{Euler angles}, \textit{axis-angle representation},
and \textit{quaternions}.\footnote{There are still other ways to parameterize rotations,
such as \textit{Rodrigues parameters}, \textit{Gibbs representation},
and \textit{Cayley–Klein parameters}.}

\subsection{Euler Angles}
\label{sec:euler_angles}
The Euler angles are three angles describing a rotation in $\mathbb{R}^{3}$.
Any rotation in $\mathbb{R}^{3}$ can be achieved by composing three \textit{elemental rotations}
(\ie rotations about the axes of a coordinate system),
and the Euler angles can be defined by three of these elemental rotations.
Rotations about the three principle axes
\textit{x}-axis, \textit{y}-axis, and \textit{z}-axis by angles
$\theta_{x}$, $\theta_{y}$, and $\theta_{z}$ are defined as follows:
\begin{equation}
\begin{array}{c}
\label{eq:RxRyRz}
\mathbf{R}_{x}(\theta_{x}) =
\begin{bmatrix}
1 & 0 & 0 \\
0 & \cos\theta_{x} & -\sin\theta_{x} \\
0 & \sin\theta_{x} & \cos\theta_{x}
\end{bmatrix},
%
\mathbf{R}_{y}(\theta_{y}) =
\begin{bmatrix}
\cos\theta_{y} & 0 & \sin\theta_{y} \\
0 & 1 & 0 \\
-\sin\theta_{y} & 0 & \cos\theta_{y}
\end{bmatrix},\\[11mm]
%
\mathbf{R}_{z}(\theta_{z}) =
\begin{bmatrix}
\cos\theta_{z} & -\sin\theta_{z} & 0 \\
\sin\theta_{z} & \cos\theta_{z} & 0 \\
0 & 0 & 1
\end{bmatrix}.
\end{array}
\end{equation}
Since matrix multiplication does not commute,
the order of the elemental rotations will affect the result.
For example, one might want to factor a rotation as
$\mathbf{R}=\mathbf{R}_{x}(\theta_{x})\mathbf{R}_{y}(\theta_{y})\mathbf{R}_{z}(\theta_{z})$,
whose ordering is $xyz$.
The rotation $\mathbf{R}$ first rotates about the \textit{z}-axis, then the \textit{y}-axis,
and finally \textit{x}-axis.
Such a sequence of rotations can be represented as the matrix product:
\begin{equation}
\label{eq:Rxyz}
\mathbf{R}(\theta_{x}, \theta_{y}, \theta_{z})=
\begin{bmatrix}
R_{11} & R_{12} & R_{13} \\
R_{21} & R_{22} & R_{23} \\
R_{31} & R_{32} & R_{33}
\end{bmatrix}=
\begin{bmatrix}
c_{y}c_{z} & -c_{y}s_{z} & s_{y} \\
c_{z}s_{x}s_{y}+c_{x}s_{z} & c_{x}c_{z}-s_{x}s_{y}s_{z} & -c_{y}s_{x} \\
-c_{x}c_{z}s_{y}+s_{x}s_{z} & c_{z}s_{x}+c_{x}s_{y}s_{z} & c_{x}c_{y}
\end{bmatrix},
\end{equation}
where we use the notation
$c_{a}=\cos(\theta_{a})$ and $s_{a}=\sin(\theta_{a})$ for $a=x,y,z$.

Starting with $R_{13}$, we find $R_{13}=s_{y}$, so $\theta_{y}=\asin(R_{13})$.
Then there are three cases to consider.
{\flushleft \bf Case 1:}
If $\theta_{y} \in (-\pi/2,\pi/2)$, then $c_{y} \neq 0$.
In this condition, $\theta_{x}=\atanT(-R_{23},R_{33})$
as $(-R_{23},R_{33})=(c_{y}s_{x}, c_{y}c_{x})$,
where $\atanT$ is the \textit{two-argument arctangent}.
In addition, $\theta_{z}=\atanT(-R_{12},R_{11})$
as $(-R_{12},R_{11})=(c_{y}s_{z}, c_{y}c_{z})$.
In summary:
\begin{equation}
\theta_{y}=\asin(R_{13}),~\theta_{x}=\atanT(-R_{23},R_{33}),~\theta_{z}=\atanT(-R_{12},R_{11}).
\end{equation}
%
{\flushleft \bf Case 2:} 
If $\theta_{y} = \pi/2$, then $s_{y}=1$ and $c_{y}=0$.
In this condition,
\begin{equation}
\begin{bmatrix}
R_{21} & R_{22} \\
R_{31} & R_{32}
\end{bmatrix}=
\begin{bmatrix}
c_{z}s_{x}+c_{x}s_{z} & c_{x}c_{z}-s_{x}s_{z} \\
-c_{x}c_{z}+s_{x}s_{z} & c_{z}s_{x}+c_{x}s_{z}
\end{bmatrix}=
\begin{bmatrix}
\sin(\theta_{z}+\theta_{x}) & \cos(\theta_{z}+\theta_{x}) \\
-\cos(\theta_{z}+\theta_{x}) & \sin(\theta_{z}+\theta_{x})
\end{bmatrix}.
\end{equation}
Therefore, $\theta_{z}+\theta_{x} = \atanT(R_{21}, R_{22})$.
Since there is only one degree of freedom, the factorization is not unique.
This ambiguity is known as \textit{gimbal lock} in applications.
In summary:
\begin{equation}
\theta_{y}=\pi/2,~\theta_{z}+\theta_{x} = \atanT(R_{21}, R_{22}).
\end{equation}
%
{\flushleft \bf Case 3:}
If $\theta_{y} = -\pi/2$, then $-s_{y}=1$ and $c_{y}=0$.
In this condition,
\begin{equation}
\begin{bmatrix}
R_{21} & R_{22} \\
R_{31} & R_{32}
\end{bmatrix}=
\begin{bmatrix}
-c_{z}s_{x}+c_{x}s_{z} & c_{x}c_{z}+s_{x}s_{z} \\
c_{x}c_{z}+s_{x}s_{z} & c_{z}s_{x}-c_{x}s_{z}
\end{bmatrix}=
\begin{bmatrix}
\sin(\theta_{z}-\theta_{x}) & \cos(\theta_{z}-\theta_{x}) \\
\cos(\theta_{z}-\theta_{x}) & -\sin(\theta_{z}-\theta_{x})
\end{bmatrix}.
\end{equation}
Therefore, $\theta_{z}-\theta_{x} = \atanT(R_{21}, R_{22})$.
It has lost one of the degrees of freedom (\ie gimbal lock).
In summary:
\begin{equation}
\theta_{y}=-\pi/2,~\theta_{z}-\theta_{x} = \atanT(R_{21}, R_{22}).
\end{equation}

We note that $xyz$ is not the only ordering.
In fact, there exist twelve possible orderings divided into two groups:
\begin{itemize}
\item Tait–Bryan angles $(xyz, yzx, zxy, xzy, zyx, yxz)$,
\item Proper Euler angles $(zxz, xyx, yzy, zyz, xzx, yxy).$
\end{itemize}
The methods of factoring a rotation according to different orderings are similar.
We refer the interested readers to~\cite{eberly2008euler} for more detailed information.

If an angle $\theta$ is close to zero, \ie $\theta \approx$ 0,
then we can use the approximations $\sin \theta \approx \theta$ and $\cos \theta \approx$ 1.
Therefore, when $\theta_{x}, \theta_{y}, \theta_{z} \approx$ 0:
\begin{equation}
\label{eq:approx_euler}
\begin{split}
\mathbf{R}\left(\theta_{x}, \theta_{y}, \theta_{z}\right)
&\approx
\begin{bmatrix}
1 & -\theta_{z} & \theta_{y} \\
\theta_{x}\theta_{y}+\theta_{z} & 1-\theta_{x}\theta_{y}\theta_{z} & -\theta_{x} \\
-\theta_{y}+\theta_{x}\theta_{z} & \theta_{x}+\theta_{y}\theta_{z} & 1
\end{bmatrix} \\
&\approx
\begin{bmatrix}
1 & -\theta_{z} & \theta_{y} \\
\theta_{z} & 1 & -\theta_{x} \\
-\theta_{y} & \theta_{x} & 1
\end{bmatrix}
=
\hat{\mathbf{R}}\left(\theta_{x}, \theta_{y}, \theta_{z}\right).
\end{split}
\end{equation}
The small angle approximation $\hat{\mathbf{R}}$ can be used in many applications requiring linear equations.
However, this approximation is no longer a rotation since
$\hat{\mathbf{R}}^{-1}\mathbf{R} \neq 1$.


\subsection{Axis–Angle Representation}
\label{sec:aa_rep}
The axis–angle representation of a rotation
parameterizes a rotation $\mathbb{R}^{3}$ by two quantities:
a 3D unit vector $\mathbf{a}$ which describes the direction of an axis of rotation,
and an angle $\theta$ which indicates the magnitude of the rotation about the axis.
This axis is also called \textit{Euler axis},
which comes from \textit{Euler's rotation theorem} stating that
any rotation or sequence of rotations of a rigid body in a 3D space
is equivalent to a pure rotation about a single rotation axis.
The angle $\theta$ scalar multiplied by the unit vector $\mathbf{a}$ is the axis-angle vector:
\begin{equation}
\mathbf{r} = \theta\mathbf{a},
\end{equation}
which is also called \textit{rotation vector} or \textit{Euler vector}.
The rotation occurs in the sense prescribed by the \textit{right-hand rule} (anticlockwise).
Compared to Euler angles, an Euler vector is simpler to compose and avoid the problem of gimbal lock.

Let $\mathbf{v}$ be a vector in $\mathbb{R}^{3}$.
After begin rotated about the axis $\mathbf{a}$ by the angle $\theta$,
the rotated vector $\hat{\mathbf{v}}$ can be computed
according to the \textit{Rodrigues' rotation formula}~\cite{wiki:rodrigues}:
\begin{equation}
\hat{\mathbf{v}}
=
\mathbf{v}\cos\theta+
(\mathbf{a}\times\mathbf{v})\sin\theta+
\mathbf{a}(\mathbf{a}\cdot\mathbf{v})(1-\cos\theta).
\end{equation}
If we represent $\hat{\mathbf{v}}$
as a matrix product of a rotation matrix $\mathbf{R}$ and the original vector $\mathbf{v}$,
then $\mathbf{R}$ can be expressed as follows~\cite{wiki:rodrigues}:
\begin{equation}
\label{eq:Rrod}
\mathbf{R}\left(\theta, \mathbf{a}\right)
=
\mathbf{I}+\sin\theta\cp{\mathbf{a}}+(1-\cos\theta)\cp{\mathbf{a}}^{2},
\end{equation}
where $\mathbf{I}$ is the 3$\times$3 identity matrix,
and $\cp{\mathbf{a}}$ denotes the \textit{cross-product matrix}
(which is also a \textit{skew-symmetric matrix})
for the vector
$\mathbf{a}=[a_{x}, a_{y}, a_{z}]^\top$:
\begin{equation}
\label{eq:cross_product_matrix}
\cp{\mathbf{a}}=
\begin{bmatrix}
0 & -a_{z} & a_{y} \\
a_{z} & 0 & -a_{x} \\
-a_{y} & a_{x} & 0
\end{bmatrix}.
\end{equation}
The matrix equation
$\cp{\mathbf{a}}\mathbf{v}=\mathbf{a}\times\mathbf{v}$
holds for any vector $\mathbf{v}$.
In~\eqnref{Rrod}, $\cp{\mathbf{a}}^{2}$ stands for the matrix product $\cp{\mathbf{a}}\cdot\cp{\mathbf{a}}$.
In addition, the rotation matrix can also be expressed in terms of the rotation vector
$\mathbf{r} = [r_{x}, r_{y}, r_{z}]^\top$:
\begin{equation}
\label{eq:R_r}
\mathbf{R}\left(\mathbf{r}\right)
=
\mathbf{I}+\left(\frac{\sin\theta}{\theta}\right)\cp{\mathbf{r}}+\left(\frac{1-\cos\theta}{\theta^{2}}\right)\cp{\mathbf{r}}^{2},
\end{equation}
where $\theta = \|\mathbf{r}\|$ is the Euclidean norm of $\mathbf{r}$.

To retrieve the axis–angle representation of a rotation matrix $\mathbf{R}$,
we first computed the angle of rotation from the trace of the rotation matrix
$\Tr(\mathbf{R})$~\cite{wiki:axisangle}:
\begin{equation}
\label{eq:rot_angle}
\theta=
\acos\left(
\frac{\Tr(\mathbf{R})-1}{2}
\right).
\end{equation}
Then the rotation axis $\mathbf{a}$ can be calculated as follows~\cite{wiki:axisangle}:
\begin{equation}
\mathbf{a}
=
\frac{1}{2\sin\theta}
\begin{bmatrix}
R_{32}-R_{23} \\
R_{13}-R_{31} \\
R_{21}-R_{12}
\end{bmatrix}.
\end{equation}

Another way to transform between rotation vectors and rotation matrices is
applying the \textit{exponential map} and its inverse, the \textit{logarithm map}.
These two techniques are presented in the theory of \textit{Lie groups},
and we refer the interested readers to~\cite{eade2013lie} for more detailed information.

If the rotation angle is very small, \ie $\theta \approx 0$,
then we can use the approximations $\sin \theta \approx \theta$ and $\cos \theta \approx 1-(\frac{\theta^{2}}{2}).$
Therefore, from~\eqnref{R_r}:
\begin{equation}
\label{eq:app_R_rv}
\mathbf{R}\left(\mathbf{r}\right)
\approx
\mathbf{I}+\cp{\mathbf{r}}+\frac{1}{2}\cp{\mathbf{r}}^{2}
\approx
\mathbf{I}+\cp{\mathbf{r}}
=
\begin{bmatrix}
1 & -r_{z} & r_{y} \\
r_{z} & 1 & -r_{x} \\
-r_{y} & r_{x} & 1
\end{bmatrix}
=
\hat{\mathbf{R}}\left(\mathbf{r}\right),
\end{equation}
which has the similar form to~\eqnref{approx_euler}.

\subsection{Quaternions}
\label{sec:quaternions}
A quaternion is a 4-tuple (\ie a vector with four components)
consisting of a complex number with three different \textit{imaginary} parts,
which gives a simple way to encode the axis–angle representation.

A quaternion $\mathbf{q}$ is generally represented in the form:
\begin{equation}
\mathbf{q} = q_{0} + q_{1}\mathbf{i} + q_{2}\mathbf{j} + q_{3}\mathbf{k},
\end{equation}
where $q_{0}$, $q_{1}$, $q_{2}$, and $q_{3}$ are real numbers,
and $\mathbf{i}$, $\mathbf{j}$, and $\mathbf{k}$ are fundamental \textit{quaternion units}.
A quaternion unit is a symbol that has no other value than itself.
By analogy with complex numbers, the term $q_{1}\mathbf{i} + q_{2}\mathbf{j} + q_{3}\mathbf{k}$
is also called the \textit{imaginary part} (or \textit{vector part}) of $\mathbf{q}$,
and $q_{0}$ is the \textit{real part} (or \textit{scalar part}) of $\mathbf{q}$.
The basic rules for multiplication are
\begin{equation}
\mathbf{i}^{2}=\mathbf{j}^{2}=\mathbf{k}^{2}=\mathbf{i}\mathbf{j}\mathbf{k}=-1,
\end{equation}
which behave similarly to the square of the imaginary unit $i$ of complex numbers.
From this follows:
\begin{equation}
\mathbf{i}\mathbf{j}=-\mathbf{j}\mathbf{i}=\mathbf{k},
\mathbf{j}\mathbf{k}=-\mathbf{k}\mathbf{j}=\mathbf{i},
\mathbf{k}\mathbf{i}=-\mathbf{i}\mathbf{k}=\mathbf{j},
\end{equation}
which behave similarly to pairwise cross products of unit vectors
$\vec{x}$, $\vec{y}$, and $\vec{z}$ in the directions
of orthogonal coordinate system axes.

For two quaternions $\mathbf{q}$ and $\mathbf{q}^{\prime}$,
their product $\mathbf{q}^{\prime\prime}=\mathbf{q}\mathbf{q}^{\prime}$,
called the \textit{Hamilton product},
is determined by the products of the quaternion units:
\begin{equation}
\label{eq:hamilton_product}
\mathbf{q}^{\prime\prime}
\equiv
\begin{bmatrix}
q_{0}^{\prime\prime}\\
q_{1}^{\prime\prime}\\
q_{2}^{\prime\prime}\\
q_{3}^{\prime\prime}
\end{bmatrix}
=
\begin{bmatrix}
q_{0} & -q_{1} & -q_{2} & -q_{3} \\
q_{1} & q_{0} & -q_{3} & q_{2} \\
q_{2} & q_{3} & q_{0} & -q_{1} \\
q_{3} & -q_{2} & q_{1} & q_{0} \\
\end{bmatrix}
\begin{bmatrix}
q_{0}^{\prime}\\
q_{1}^{\prime}\\
q_{2}^{\prime}\\
q_{3}^{\prime}
\end{bmatrix}.
\end{equation}
It should be noted that 
the multiplication of quaternions is associative and distributes over vector addition,
but it is not commutative. We list other quaternion properties as follows:
\begin{itemize}
\item Norm: $\|\mathbf{q}\|^{2}=q_{0}^{2}+q_{1}^{2}+q_{2}^{2}+q_{3}^{2}$,
\item Conjugate quaternion: $\bar{\mathbf{q}}=q_{0}-q_{1}\mathbf{i}-q_{2}\mathbf{j}-q_{3}\mathbf{k}$,
\item Inverse quaternion: $\mathbf{q}^{-1}=\frac{\bar{\mathbf{q}}}{|\mathbf{q}|^{2}}$,
\item Unit quaternion: $\|\mathbf{q}\|=1,$
\item Inverse of unit quaternion: $\mathbf{q}^{-1}=\bar{\mathbf{q}}$.
\end{itemize}

Rotation through an angle of $\theta$ around the unit rotation axis
$\mathbf{a}=[a_{x}, a_{y}, a_{z}]^\top$ can be represented
by a \textit{unit quaternion} (or \textit{rotation quaternion}):
\begin{equation}
\mathbf{q}
=
e^{\frac{\theta}{2}(a_{x}\mathbf{i}+a_{y}\mathbf{j}+a_{z}\mathbf{k})}
=
\cos\frac{\theta}{2}+(a_{x}\mathbf{i}+a_{y}\mathbf{j}+a_{z}\mathbf{k})\sin\frac{\theta}{2},
\end{equation}
and the detailed derivation can be found in~\cite{kuipers2011quaternions}.
In this case, $q_{0}$, $q_{1}$, $q_{2}$, and $q_{3}$ equal to
$\cos\frac{\theta}{2}$, $a_{x}\sin\frac{\theta}{2}$, $a_{y}\sin\frac{\theta}{2}$, and $a_{z}\sin\frac{\theta}{2}$, respectively.
The desired rotation can be applied to a 3D vector $\mathbf{p}$
by evaluating the \textit{conjugation} of $\mathbf{p}$ by $\mathbf{q}$ using the Hamilton product:
\begin{equation}
\mathbf{p}^{\prime}=\mathbf{q}\mathbf{p}\mathbf{q}^{-1},
\end{equation}
where $\mathbf{p}$ and $\mathbf{p}^{\prime}$ are represented by quaternions with zero real parts:
\begin{equation}
\mathbf{p} = p_{x}\mathbf{i} + p_{y}\mathbf{j} + p_{z}\mathbf{k},~~~~~
\mathbf{p}^{\prime} = p_{x}^{\prime}\mathbf{i} + p_{y}^{\prime}\mathbf{j} + p_{z}^{\prime}\mathbf{k},
\end{equation}
and $\mathbf{p}^{\prime}$ is the new vector after the rotation.
It follows that conjugation by the product of two quaternions
is the composition of conjugations by these quaternions.
For instance, if $\mathbf{q}_{1}$ and $\mathbf{q}_{2}$ are unit quaternions,
then the rotation (\ie conjugation) by $\mathbf{q}_{1}\mathbf{q}_{2}$ is:
\begin{equation}
\mathbf{q}_{1}\mathbf{q}_{2}\mathbf{p}(\mathbf{q}_{1}\mathbf{q}_{2})^{-1}
=
\mathbf{q}_{1}\mathbf{q}_{2}\mathbf{p}\mathbf{q}_{2}^{-1}\mathbf{q}_{1}^{-1}
=
\mathbf{q}_{1}(\mathbf{q}_{2}\mathbf{p}\mathbf{q}_{2}^{-1})\mathbf{q}_{1}^{-1},
\end{equation}
which is the same as rotating (\ie conjugating) $\mathbf{p}$ by
$\mathbf{q}_{2}$ and then by $\mathbf{q}_{1}$.
The real part of the result is necessarily zero.
In this case, the two rotation quaternions can also be first
combined into one equivalent quaternion by the relation
$\mathbf{q}^{\prime}=\mathbf{q}_{1}\mathbf{q}_{2}$,
where $\mathbf{q}^{\prime}$ corresponds to the rotation
$\mathbf{q}_{2}$ followed by the rotation $\mathbf{q}_{1}$.

A quaternion rotation
$\mathbf{p}^{\prime}=\mathbf{q}\mathbf{p}\mathbf{q}^{-1}$
can also be algebraically manipulated into a matrix rotation
$\mathbf{p}^{\prime}=\mathbf{R}\mathbf{p}$,
in which $\mathbf{R}$ is the rotation matrix:
\begin{equation}
\mathbf{R}\left(q_{0}, q_{1}, q_{2}, q_{3}\right)
=
\begin{bmatrix}
q_{0}^{2}+q_{1}^{2}-q_{2}^{2}-q_{3}^{2} & 2q_{1}q_{2}-2q_{0}q_{3} & 2q_{1}q_{3}+2q_{0}q_{2} \\
2q_{1}q_{2}+2q_{0}q_{3} & q_{0}^{2}-q_{1}^{2}+q_{2}^{2}-q_{3}^{2} & 2q_{2}q_{3}-2q_{0}q_{1} \\
2q_{1}q_{3}-2q_{0}q_{2} & 2q_{2}q_{3}+2q_{0}q_{1} & q_{0}^{2}-q_{1}^{2}-q_{2}^{2}+q_{3}^{2}
\end{bmatrix}.
\end{equation}
When converting a rotation matrix to a quaternion,
several straightforward methods tend to be unstable
when the trace of the rotation matrix is very close to zero.
We refer the interested readers to~\cite{van2005quaternion}
for a more stable method of converting a rotation matrix to a quaternion.

The rotation axis $\mathbf{a}$ and angle $\theta$
corresponding to a quaternion
$\mathbf{q} = q_{0} + q_{1}\mathbf{i} + q_{2}\mathbf{j} + q_{3}\mathbf{k}$
can be extracted as follows:
\begin{equation}
\label{eq:qua_to_aa}
(a_{x},a_{y},a_{z})=\frac{(q_{1},q_{2},q_{3})}{\sqrt{1-q_{0}^{2}}},~~~~~
\theta=2~\acos(q_{0}),
\end{equation}
where $\acos(\cdot)$ is the \textit{arccosine} function.
Please note that when the quaternion approaches a real quaternion
(\ie a quaternion with zero imaginary part),
the axis is not well-defined due to \textit{degeneracy}.

\section{Evaluation Metrics}
\label{sec:evaluation_metrics}
In order to evaluate the performance of between different object pose recovering methods properly,
we have to use an appropriate metric first.
In fact, there have been many metrics defined for evaluating object pose estimation and tracking methods,
which will be introduced next.
In the following paragraphs, we use $\mathbf{p} \equiv (\mathbf{R}, \mathbf{t})$
and $\tilde{\mathbf{p}} \equiv (\tilde{\mathbf{R}}, \tilde{\mathbf{t}})$
as the estimated pose and the ground-truth pose, respectively,

\subsection{Rotation \& Translation Errors}
\label{sec:metric_rt}
The rotation error is defined as the angle of the relative rotation 
between the estimated and ground-truth rotations.
While the rotations are represented as matrices,
we can get the estimated rotation error according to~\eqnref{rot_angle}:
\begin{equation}
\label{eq:rot_mat_err}
E_{r}(\mathbf{R})=\acos\left(\frac{\Tr(\Delta\mathbf{R})-1}{2}\right),
\end{equation}
where $\Delta\mathbf{R} = \mathbf{R}^{-1}\cdot\tilde{\mathbf{R}} = \mathbf{R}^{\top}\cdot\tilde{\mathbf{R}}$
is the relative rotation matrix.
In addition, if the rotations are represented as unit quaternions
(\ie $\mathbf{q}$ and $\tilde{\mathbf{q}}$),
then we can efficiently compute the rotation error as follows:
\begin{equation}
\label{eq:rot_qua_err}
E_{r}(\mathbf{q})=2~\acos\left(\dotP(\mathbf{q},\tilde{\mathbf{q}})\right),
\end{equation}
where
$\dotP(\mathbf{q},\tilde{\mathbf{q}})=q_{0}\tilde{q}_{0}+q_{1}\tilde{q}_{1}+q_{2}\tilde{q}_{2}+q_{3}\tilde{q}_{3}$
is the dot product of $\mathbf{q}$ and $\tilde{\mathbf{q}}$.
The reason behind~\eqnref{rot_qua_err} is that the relative rotation is represented as follows:
\begin{equation}
\Delta\mathbf{q}=
\mathbf{q}^{-1}\tilde{\mathbf{q}}=
\bar{\mathbf{q}}\tilde{\mathbf{q}}=
(q_{0}-q_{1}\mathbf{i}-q_{2}\mathbf{j}-q_{3}\mathbf{k})
(\tilde{q}_{0}+\tilde{q}_{1}\mathbf{i}+\tilde{q}_{2}\mathbf{j}+\tilde{q}_{3}\mathbf{k}).
\end{equation}
And the rotation angle of $\Delta\mathbf{q}$ is
$2~\acos(\delta q_{0})$ according to~\eqnref{qua_to_aa},
in which $\delta q_{0}$ is the real part of $\Delta\mathbf{q}$
and is equivalent to $\dotP(\mathbf{q},\tilde{\mathbf{q}})$.

The translation error can be defined as either the \textit{absolute difference} between
the estimated and ground-truth translations directly in physical units (\eg meters):
\begin{equation}
\label{eq:tra_abs}
E_{t}(\mathbf{t})=\|\mathbf{t}-\tilde{\mathbf{t}}\|,
\end{equation}
or the \textit{relative difference} in percentage terms:
\begin{equation}
\label{eq:tra_rel}
E_{t}(\mathbf{t})=\frac{\|\mathbf{t}-\tilde{\mathbf{t}}\|}{\|\tilde{\mathbf{t}}\|}\times100\%.
\end{equation}

{\flushleft \bf Success Rate (SR).}
We define a pose to be successfully estimated
if its rotation and translation errors are both under predefined thresholds.
For example, Shotton \etal~\cite{shotton2013scene} and Tseng \etal~\cite{tseng2016direct}
use the thresholds of 5\degree\&5cm and 20\degree\&10\%, respectively.
The success rate according to the metric of \textit{rotation \& translation errors} is defined
as the percentage of the successfully estimated poses.
Sometimes we also use measures of the average rotation and translation errors,
and they computed only for successfully estimated poses.

\subsection{3D Distance}
\label{sec:metric_3d}
This metric is used to compute the averaged 3D distance between
points transformed using the estimated pose and the ground-truth pose~\cite{hinterstoisser2012model}:
\begin{equation}
\label{eq:pose_3d_err}
E_{3D}=\frac{1}{m}\sum_{\mathbf{x}\in \mathcal{M}}\|\mathbf{R}\mathbf{x}+\mathbf{t}-(\tilde{\mathbf{R}}\mathbf{x}+\tilde{\mathbf{t}})\|,
\end{equation}
where $\mathbf{x}$ is a 3D point of the target object model,
$m$ is the number of points on the model,
and $\mathcal{M}$ is the set of all 3D points of this model.

{\flushleft \bf Success Rate (SR).}
For the metric of \textit{3D Distance}, 
a pose is considered to be successfully estimated
if the 3D distance error $E_{3D}$ is less than the product of $kd$,
where $d$ is the diameter
(\ie the longest distance between vertices) of $\mathcal{M}$ and $k$ is a pre-defined threshold.
In~\cite{hinterstoisser2012model}, Hinterstoisser \etal set the value of $k$ to be 0.1,
which means the computed average distance is within 10\% of the model diameter.

\subsection{2D Projection}
\label{sec:metric_2d}
Sometimes the previous measures are not very well suited
when applying visual effects to 2D images (\eg in AR applications).
For instance, the translation accuracy in $z$-axis is less critical for the visual impression than 
the accuracy in $x$-axis and $y$-axis.
This metric focuses on the matching of pose estimation on 2D images~\cite{brachmann2016uncertainty}:
\begin{equation}
\label{eq:pose_2d_err}
E_{2D}=\frac{1}{m}\sum_{\mathbf{x}\in \mathcal{M}}\|\mathbf{K}(\mathbf{R}\mathbf{x}+\mathbf{t})-\mathbf{K}(\tilde{\mathbf{R}}\mathbf{x}+\tilde{\mathbf{t}})\|,
\end{equation}
where $\mathbf{x}$ is a 3D point of the target object model,
$m$ is the number of points on the model,
$\mathcal{M}$ is the set of all 3D points of this model,
and $\mathbf{K}$ is the intrinsic matrix of the camera as defined in~\eqnref{in_ex}.

{\flushleft \bf Success Rate (SR).}
We say that a pose is correctly estimated
if the 2D projection error $E_{2D}$ is less than a pre-defined threshold.
Brachmann \etal~\cite{brachmann2016uncertainty} use 5px (\ie 5 pixels) as the threshold
for the metric of \textit{2D projection}.


