list = dir('*.png');
for i = 1:size(list, 1)
    img = imread(list(i).name);
    [~, name, ~] = fileparts(list(i).name);
    imwrite(img, [name, '.jpg']);
end