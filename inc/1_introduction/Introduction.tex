\chapter{Introduction}
\label{ch:intro}
Determining the pose of a
target object from a calibrated camera is a classical problem in
computer vision that finds numerous applications such as robotics,
augmented reality (AR), and virtual reality (VR).
For the case of a rigid body, its pose can
be described by a six degrees of freedom (6DoF) transformation,
consisting of three position parameters and three orientation parameters.
While much progress has been made in the past decade,
it remains a challenging task to develop a fast and accurate pose estimation algorithm.

In general, object pose estimation refers to
computing the position and orientation of a target object given a single-view image.
The target object is with prior knowledge (\eg shape or texture) in most cases.
On the contrary, object pose tracking indicates determining the poses of an object
in an ordered sequence of camera frames.
In this case, the object pose in a previous frame is already known, and thus
one can exploit this information when computing the object pose in a current frame.
Furthermore, it is also applicable to recover the object pose with multiple views,
which may achieve superior pose estimation results.

\begin{figure}
\vergap{0.5}
\horgap{2pt}
\begin{center}
\begin{tabular}{@{}cccc@{}}
\includegraphics[width=0.24\linewidth]{inc/1_introduction/figure/introduction_opt/1-1} &
\includegraphics[width=0.24\linewidth]{inc/1_introduction/figure/introduction_opt/1-2} &
\includegraphics[width=0.24\linewidth]{inc/1_introduction/figure/introduction_opt/2-1} &
\includegraphics[width=0.24\linewidth]{inc/1_introduction/figure/introduction_opt/2-2} \\
\includegraphics[width=0.24\linewidth]{inc/1_introduction/figure/introduction_opt/1-3} &
\includegraphics[width=0.24\linewidth]{inc/1_introduction/figure/introduction_opt/1-4} &
\includegraphics[width=0.24\linewidth]{inc/1_introduction/figure/introduction_opt/2-3} &
\includegraphics[width=0.24\linewidth]{inc/1_introduction/figure/introduction_opt/2-4} \\
\includegraphics[width=0.24\linewidth]{inc/1_introduction/figure/introduction_opt/1-5} &
\includegraphics[width=0.24\linewidth]{inc/1_introduction/figure/introduction_opt/1-6} &
\includegraphics[width=0.24\linewidth]{inc/1_introduction/figure/introduction_opt/2-5} &
\includegraphics[width=0.24\linewidth]{inc/1_introduction/figure/introduction_opt/2-6}
\end{tabular}
\end{center}
\caption{
Images of 2D (left two columns) and 3D objects (right two columns) in our benchmark dataset
with 6DoF pose ground-truth notation.
%
The proposed benchmark dataset contains 690 color and depth videos of various
textured and geometric objects with over 100,000 frames.
%
The recorded sequences also include image distortions for performance evaluation in real-world scenarios.
}
\label{fig:introduction_opt}
\end{figure}


In this dissertation, we primarily discuss how to compute the pose of a target object
accurately and robustly with a single view.
In particular, 
we propose a large-scale object pose tracking benchmark dataset
consisting of RGB-D video sequences of 2D and 3D targets with ground-truth information,
as shown in
\figref{introduction_opt}.\footnote{All the images in the dissertation are used under Creative Commons license.}
%
Furthermore, we perform the thorough quantitative evaluation
of the state-of-the-art methods on this benchmark dataset.
%
We observe that while advanced Perspective-$n$-Point (P$n$P) algorithms perform well in pose estimation,
the success hinges on whether feature points can be extracted and matched
correctly on target objects with rich texture.
%
Consequently, we develop a two-step robust direct method for 6DoF pose estimation
that performs accurately on both textured and textureless planar target objects.
%
First, the pose of a planar target object with respect to a calibrated camera
is approximately estimated by posing it as a template matching problem.
Second, each object pose is refined and disambiguated using a dense alignment scheme,
as illustrated in~\figref{introduction_dpe}.
%
Based on the proposed two-step direct method,
we present a system for real-time 6DoF tracking of a passive stylus
that achieves submillimeter accuracy,
which is suitable for writing or drawing in mixed reality applications,
as demonstrated in~\figref{introduction_dodecapen}.
We demonstrate the system performance regarding speed and accuracy on a number of synthetic and real datasets,
showing that it can be competitive with state-of-the-art multi-camera motion capture systems.
%
We also demonstrate several applications of the technology ranging from 2D and 3D drawing in VR to general object
manipulation and board games.

\begin{figure}
\vergap{0.25}
\horgap{1pt}
\begin{center}
\begin{tabular}{@{}cccccccc@{}}
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/bu_j4_06_1.jpg} &
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/st_j3_05_1.jpg} &
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/lu_i5_01_1.jpg} &
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/ma_i1_47_1.jpg} &
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/is_j3_18_1.jpg} &
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/ph_i1_43_1.jpg} &
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/gr_i4_12_1.jpg} &
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/wa_b1_17_1.jpg}
\\
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/bu_j4_06_2.jpg} &
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/st_j3_05_2.jpg} &
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/lu_i5_01_2.jpg} &
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/ma_i1_47_2.jpg} &
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/is_j3_18_2.jpg} &
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/ph_i1_43_2.jpg} &
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/gr_i4_12_2.jpg} &
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/wa_b1_17_2.jpg}
\\
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/bu_j4_06_3.jpg} &
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/st_j3_05_3.jpg} &
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/lu_i5_01_3.jpg} &
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/ma_i1_47_3.jpg} &
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/is_j3_18_3.jpg} &
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/ph_i1_43_3.jpg} &
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/gr_i4_12_3.jpg} &
\includegraphics[width=0.12\linewidth]{inc/1_introduction/figure/introduction_dpe/wa_b1_17_3.jpg}
\end{tabular}
\end{center}
\caption{
Direct pose estimation for planar targets.
%
The pose ambiguity problem occurs when the objective function has several local minima for a given configuration,
which is the primary cause of flipping estimated poses.
%
First row: original images.
Second row: images rendered with a box model according to the ambiguous pose
obtained from proposed algorithm without refinement approach.
%
Third row: pose estimation results from the proposed algorithm, which can disambiguate plausible poses effectively.
}
\label{fig:introduction_dpe}
\end{figure}

\begin{figure}[!]
\horgap{1pt}
\begin{center}
\footnotesize
\begin{tabular}{@{}cccc@{}}
\includegraphics[width=0.129\linewidth]{inc/1_introduction/figure/introduction_dodecapen/01} &
\includegraphics[width=0.261\linewidth]{inc/1_introduction/figure/introduction_dodecapen/02} &
\includegraphics[width=0.270\linewidth]{inc/1_introduction/figure/introduction_dodecapen/03} &
\includegraphics[width=0.270\linewidth]{inc/1_introduction/figure/introduction_dodecapen/04} \\
(a) DodecaPen &
(b) Monocular video &
(c) DodecaPen tracking &
(d) Ground-truth scan
\end{tabular}
\end{center}
\caption{
Our proposed system can track the 6DoF pose of
(a) a calibrated pen (the DodecaPen) from
(b) a single camera with submillimeter accuracy.
We show (c) a digital 2D drawing as the visualization of the tracking result,
and compare with (d) a scan of the actual drawing.
}
\label{fig:introduction_dodecapen}
\end{figure}



\section{Object Pose Recovering}
\label{sec:obj_pose}
In computer vision and robotics, it is a typical task to identify some specific object in a camera image
and estimate its position and orientation relative to the camera coordinate system.
We regard this process as \textit{object pose estimation} if the input data is only an image.
When referring to \textit{object pose tracking},
the input image data would be an ordered sequence of camera frames instead.
This type of task can also be called \textit{outside-in tracking}, where the target object is observed
from outside by the camera system.
And since the object model is known before computing its pose, it is also called \textit{model-based tracking}.

An object is regarded as a \textit{rigid body} if the 
distance between any two given points on it remains constant in time regardless of external forces exerted on it.
For a rigid body, its position and orientation in space are defined by three components of translation
and three components of rotation, which means that it has six degrees of freedom.
In contrast, the freedom of movement of a \textit{non-rigid body} (\eg the human hand) may be more than six.
In this dissertation, we focus on recovering the pose of an object which is a rigid body.

Existing algorithms for recovering the 6DoF pose of an object
can be broadly categorized into three main approaches:
%

\noindent {\bf Direct approaches}~\cite{hinterstoisser2012model,tseng2016direct}.
These approaches address the problem by finding the best fit from numerous pre-determined candidates
based on the holistic template or appearance matching.
The corresponding pose of the best candidate is considered as the estimation result.
%

\noindent {\bf Feature-based approaches}~\cite{lepetit2004point,collet2009object,tang2012textured}.
The core idea is to first establish a set of feature correspondences between
the target object and projected camera frame~\cite{lowe2004distinctive,yu2011asift}.
%
Outliers are then removed to obtain reliable feature pairs~\cite{fischler1981random},
and the final pose is computed with P$n$P algorithms~\cite{lepetit2009epnp,zheng2013revisiting}.
%
In contrast to direct methods, the performance of feature-based methods
depends on whether both features can be extracted and matched well.
%

\noindent {\bf Learning-based approaches}~\cite{tejani2014latent,
brachmann2014learning,brachmann2016uncertainty,kehl2016deep}.
%
These methods learn an abstract representation of an object from a set of images captured
from different viewpoints,
from which the pose of the target in a new input frame is determined.
%
While feature-based and direct methods are more effective for textured and non-occluded objects respectively,
learning-based approaches have shown the potential to track poses of objects with diverse textures
under partial occlusion.

Real-time pose tracking can be accomplished by leveraging the information obtained
from previous frames~\cite{park2011texture,prisacariu2012pwp3d,choi13rgbd,tjaden2016real}.
%
In addition, the pose estimation task
can be accelerated by exploiting a small search range within the camera viewpoint
or reducing the number of pose candidates.
%
To prevent pose jittering during the tracking process,
which is indispensable especially in AR applications~\cite{marchand2016pose},
further pose refinement should be performed.
We refer the interested readers to~\cite{billinghurst2015survey} for more information on AR.

To evaluate existing pose estimation algorithms, many benchmark datasets 
have been proposed~\cite{hinterstoisser2012model,choi2013rgb,tejani2014latent,
brachmann2014learning,krull20146,rennie2016dataset}.
%
However, there are two main issues that need to be addressed.
%
First, while the datasets are mainly designed for single-frame based pose estimation,
most images do not contain distortions (\eg motion blur caused by different object or camera motions)
that are crucial for performance evaluation for real-world scenarios.
%
Second, the camera trajectories in most datasets are not carefully designed (\ie freestyle motion),
which do not allow detailed analysis for specific situations.
%
Most importantly, it is of great interest for fields of computer vision
to develop an extensive benchmark dataset for thorough performance evaluation
of 6DoF pose tracking in real-world scenarios.

\section{Camera Pose Recovering}
\label{sec:cam_pose}
In contrast to the 6DoF object pose, the 6DoF camera pose
denotes the camera's position and orientation with respect to the object (or world) coordinate system.
%
Though the object pose and the camera pose merely have the inverse transformation relationship to each other,
the approaches of between object pose recovering, and camera pose recovering are still quite different.
%
In general, we exploit the information extracted from the entire camera image to compute the camera pose.
But when determining the object pose, only a subregion of the image where the object locates
contributes to the final pose estimation result.

The task of camera pose recovering from visual data can be broadly subdivided into four categories,
according to whether or not it is processed online,
and if a map of the environment is built concurrently.
We present the four types in~\tabref{camera_pose_recovering_four_cat}
and provide the corresponding descriptions below.

\begin{table}
\vergap{0.8}
\horgap{4.5pt}
\caption{Four categories of camera pose recovering problem.}
\vspace{5pt}
\label{tab:camera_pose_recovering_four_cat}
\centering
\footnotesize 
\begin{tabular}{ccc}
\toprule
& With Mapping & Without Mapping \\
\midrule
Online & Simultaneous Localization and Mapping (SLAM) & Visual Odometry (VO) \\
Offline & Structure from Motion (SfM) & Image-based Localization (IBL) \\
\bottomrule
\end{tabular}
\end{table}

\noindent {\bf Simultaneous Localization and Mapping (SLAM)}
\cite{durrant2006simultaneous,bailey2006simultaneous,mur2015orb}.
SLAM techniques build a map of an unknown environment and
localize the sensor in the map simultaneously in real-time.
%
Among different sensor types,
a camera is the most common one as it can provide rich information about the environment
that allows robust and accurate localization and mapping.
The SLAM approach which uses a camera as the primary sensor is denoted as \textit{Visual SLAM}, and
it has been a hot research topic in the last years
\cite{davison2007monoslam,klein2007parallel,newcombe2011dtam,engel2014lsd,mur2017orb}.

\noindent {\bf Visual Odometry (VO)}~\cite{nister2004visual,scaramuzza2011visual,forster2014svo}.
VO is the process of determining the location of a camera by analyzing the associated camera images in real-time.
The main difference between VO and SLAM is that VO mainly addresses itself on
\textit{local consistency} and focuses on incrementally estimating the path of the camera pose after pose.
Nevertheless, SLAM aims to achieve a \textit{globally consistent} estimate of the camera trajectory and map
by realizing that a previously mapped area has been re-visited and this information
is used to reduce the drift in the estimates by \textit{loop closure} techniques
\cite{yousif2015overview,engel2018direct}.

\noindent {\bf Structure from Motion (SfM)}~\cite{longuet1981computer,agarwal2011building,ozyecsil2017survey}.
SfM approaches recovers the map of an environment from a set of projective measurements,
represented as a collection of 2D images,
via estimation of the camera poses corresponding to these images.
And the images can be either ordered or not.
The \textit{bundle adjustment} methods~\cite{agarwal2010bundle},
which aim to determine the map and the camera poses simultaneously
that minimize the discrepancy between
image measurements and the reconstructed model,
are usually used to solve the SfM problem
\cite{snavely2008skeletal,havlena2010efficient}.

\noindent {\bf Image-Based Localization (IBL)}~\cite{irschara2009structure,sattler2012improving,sun2017dataset}.
IBL methods address the problem of finding the camera pose from which a camera image is taken.
Traditionally, large-scale IBL has been treated as an image retrieval problem.
After finding images in a database that are most similar to the query image,
the location of the query image can be recovered with respect to them~\cite{schindler2007city}.
However, the localization accuracy obtained this way cannot be very satisfactory.
Recently, IBL algorithms benefit from a 3D reconstruction of the scene produced by SfM or SLAM,
which the query images can be accurately registered to~\cite{snavely2006photo,sattler2012improving,li2012worldwide}.

\section{Cameras}
\label{sec:cams}
In vision-based applications, camera images are the necessary data
when estimating either the object pose or the camera pose.
Generally speaking, the cameras used in the pose recovering problem can be classified into three categories,
namely monocular camera, stereo camera, and depth camera.
And each pose recovering algorithm is designed according to a specific class of cameras.

\noindent {\bf Monocular Camera}.
It is the most common type of camera with one lens and a corresponding image sensor or film frame.
The image sensor can be either monochrome (\ie grayscale) one
or color (\ie RGB, which stands for red, green, and blue) one.
Parameters of the lens and the images sensor of a camera
can be estimated by performing \textit{camera calibration}~\cite{zhang2000flexible},
which is also called \textit{camera resectioning} or
\textit{geometric camera calibration}.\footnote{The process is different from \textit{photometric camera calibration} (\ie color mapping).}
These parameters consist of camera \textit{intrinsic parameters} and \textit{distortion coefficients}.
The intrinsic parameters encompass focal length, image sensor format, and principal point.
In addition, the distortion coefficients include radial distortion and tangential distortion coefficients.
In most 3D computer vision tasks, 
camera calibration is the first step before applying further algorithms like pose estimation and tracking.
%
Moreover, cameras equipped with global shutter are much more preferable
than those provided with rolling shutter because the latter can cause undesirable effects such as
wobble, skew, spatial aliasing, and temporal aliasing~\cite{liang2008analysis}.

\noindent {\bf Stereo Camera}.
This type of camera has two or more lenses with a separate image sensor for each lens.
This configuration allows the camera to simulate human binocular vision
and therefore gives it the ability to capture depth information.
Consequently, a stereo camera can also be regarded as a depth camera,
which will be presented in the next paragraph.
One well-known technique applied with the images captured
by a stereo camera is \textit{stereo matching}~\cite{sun2003stereo,shi2015high},
by which the depth information of a scene can be acquired.
This is also called \textit{stereoview} or \textit{stereoscopic} in computer vision.
Different from doing single camera calibration which is mentioned in the previous paragraph, 
we should perform stereo camera calibration to estimate not only the 
intrinsic parameters and distortion coefficients of each lens, but also the 
relative poses between lenses.

\noindent {\bf Depth Camera}.
A depth camera (or \textit{range camera}) is a \textit{range imaging} device
that measures the distance from the camera to points in a scene, and as shown in~\figref{introduction_depth}.
The resulting image is called \textit{range image} or \textit{depth image},
which has pixel values corresponding to the measured distance.
If the depth camera is accurately calibrated, then the pixel values
can be given directly in physical units, such as millimeters applied by
Microsoft Kinect V1~\cite{zhang2012microsoft} and Microsoft Kinect V2~\cite{yang2015evaluating}.
Depth cameras can operate according to numerous techniques, such as
\textit{stereo triangulation}~\cite{davis2003spacetime},
\textit{structured light}~\cite{scharstein2003high} (\eg Microsoft Kinect V1), and 
\textit{time-of-flight}~\cite{gokturk2004time} (\eg Microsoft Kinect V2).
Furthermore, depth cameras using either of the latter two techniques should be equipped with
an infrared (IR) projector and an IR camera.

\begin{figure}
\begin{center}
\includegraphics[width=0.7\linewidth]{inc/1_introduction/figure/depth/depth.pdf}
\end{center}
\caption{
The depth data is measured from scene points to the camera plane
on where the camera is
(instead of from scene points to camera center).
}
\label{fig:introduction_depth}
\end{figure}

\section{Contributions}
\label{sec:contributions}
In this dissertation,
we propose a benchmark dataset for 6DoF object pose tracking,
which consists of 690 videos under seven varying conditions with five speeds.
%
It is a large-scale dataset where images are acquired
from a moving camera for performance evaluation of both 2D and 3D object pose tracking algorithms.
%
The proposed dataset can be used in other computer vision tasks such as
3D feature tracking and matching as well.
%
Furthermore, we extensively evaluate and analyze each pose tracking method
employing more than 100,000 frames including both 2D and 3D objects.
%
Since the advanced SLAM methods~\cite{mur2017orb,whelan2016elasticfusion}
are able to track and relocalize camera pose in real time,
we also evaluate these approaches by adapting them to object pose tracking scenarios.
%
We present the extensive performance evaluation of
the state-of-the-art methods using the proposed benchmark dataset
and discuss the potential research directions in this field.
The OPT datasets are available on our project website
at~\href{http://media.ee.ntu.edu.tw/research/OPT}{\color{magenta}{media.ee.ntu.edu.tw/research/OPT}}.

From our observation, since the performance of feature-based methods hinges on
whether or not point correspondences can be correctly established,
these approaches are less effective when the target images contain
less textured surfaces or motion blurs.
Therefore, we propose an efficient direct pose estimation (DPE) algorithm
for planar targets undergoing arbitrary 3D perspective transformations.
%
The DPE algorithm performs favorably against the state-of-the-art
feature-based approaches in terms of robustness and accuracy
on both textured and textureless planar target objects.
%
In addition, we demonstrate the proposed pose refinement technique
not only improves the accuracy of estimated results but also alleviates
the pose ambiguity problem effectively.
The source code of the DPE method and the related datasets are available on our project website
at~\href{http://media.ee.ntu.edu.tw/research/DPE}{\color{magenta}{media.ee.ntu.edu.tw/research/DPE}}.

Based on the pose refinement technique proposed in the DPE method,
we develop a 6DoF tracking system called DodecaPen that requires only a single off-the-shelf camera
and a passive 3D-printed fiducial with several
hand-glued binary square markers printed from a laser printer.
We show that off-the-shelf fiducial tracking with markers is insufficient for achieving the
accuracy necessary for digital 2D drawing.
Instead, our system consists of the following components:
(a) a 3D printed dodecahedron with hand-glued binary square markers mechanically designed for pose estimation,
(b) a one-time calibration procedure for the (imprecise) model using bundle adjustment,
(c) approximate pose estimation from fiducial corners,
(d) inter-frame fiducial corner tracking, and
(e) dense pose refinement by direct model-image alignment.
We show that each step of the above system is essential to robust tracking and that the combined
system allows us to achieve an absolute accuracy of 0.4 mm from a single camera,
which is comparable to state-of-the-art professional motion capture (mocap) systems.
We rigorously evaluate the performance of the proposed method
when we degrade the camera
(with shot noise, spatial blur, and reduced spatial resolution).
Thorough evaluation results can be found on our project website
at~\href{http://media.ee.ntu.edu.tw/research/DodecaPen}{\color{magenta}{media.ee.ntu.edu.tw/research/DodecaPen}}.
We conclude with demonstrations of this accurate and easy-to-setup 6DoF status
tracking system for the application of drawing in 2D and 3D as well as object manipulation
in a virtual reality (VR) environment.


\section{Publications}
\label{sec:publications}
The core of the dissertation relies on the following peer-reviewed publications:
\begin{itemize}
\item
Po-Chen Wu, Yueh-Ying Lee, Hung-Yu Tseng, Hsuan-I Ho, Ming-Hsuan Yang, and Shao-Yi Chien,
``\textbf{A Benchmark Dataset for 6DoF Object Pose Tracking}."
In \textit{Proceedings of the IEEE International Symposium on Mixed and Augmented Reality (ISMAR Adjunct)},
2017.~\cite{wu2017benchmark}
\item
Po-Chen Wu, Hung-Yu Tseng, Ming-Hsuan Yang, and Shao-Yi Chien,
``\textbf{Direct Pose Estimation for Planar Objects}."
In \textit{Computer Vision and Image Understanding},
2018.~\cite{wu2018direct}
\item
Po-Chen Wu, Robert Wang, Kenrick Kin, Christopher Twigg, Shangchen Han, Ming-Hsuan Yang, and Shao-Yi Chien,
``\textbf{DodecaPen: Accurate 6DoF Tracking of a Passive Stylus}."
In \textit{Proceedings of the ACM Symposium on User Interface Software and Technology (UIST)},
2017.
\textit{\textbf{(ACM UIST Honorable Mention Award)}}~\cite{wu2017dodecapen}
\end{itemize}

\section{Dissertation Organization}
\label{sec:diss_org}
We formulate the pose recovering problem in the subsequent chapter.
In particular, we introduce the vectorial parameterization of rotation
and different evaluation metrics used for pose recovering problem.
%
In~\chref{related_work}, we give an overview of the related work
which is relevant for the remainder of the dissertation.
%
Furthermore, we discuss methods of object pose recovering,
and existing benchmark datasets proposed in the literature.
%
Afterwards, we present a large-scale object pose tracking benchmark dataset of
RGB-D video sequences for both 2D and 3D objects,
as well as extensive quantitative evaluation results of the
state-of-the-art methods on this dataset in~\chref{opt}.
%
In~\chref{dpe}, we propose a two-step direct pose estimation algorithm for planar objects.
%
Then, in~\chref{dodecapen}, we present a system for real-time 6DoF tracking of a passive stylus
that achieves submillimeter accuracy,
which is suitable for writing or drawing in mixed reality applications.
%
Finally, we conclude this dissertation with discussions on future work in~\chref{conclusion}.
