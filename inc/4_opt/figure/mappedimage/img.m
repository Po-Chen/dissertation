depth = double(imread('mappeddepth.png'));
depth(depth==0) = nan;
a = min(min(depth));
depth(isnan(depth)) = a/2;
depth = mat2gray(depth);
imshow(depth);
imwrite(depth, 'mappedimage_1-2.png');


depth = double(imread('depth.png'));
depth(depth==0) = nan;
a = min(min(depth));
depth(isnan(depth)) = a/2;
depth = mat2gray(depth);
imshow(depth);
imwrite(depth, 'mappedimage_2-1.png');