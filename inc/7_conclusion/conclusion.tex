\chapter{Conclusion}
\label{ch:conclusion}
In this dissertation,
we have interpreted the formulation of the 6DoF object pose recovering problem in~\chref{prob_formul}
and given a comprehensive introduction to the related work of this topic in~\chref{related_work}.
Previous approaches have been thoroughly analyzed and
a number of new techniques have been presented in~\chref{opt} to~\chref{dodecapen}.
The main achievements are:

\begin{itemize}

\item
A large-scale object pose tracking benchmark dataset
consisting of RGB-D video sequences of 2D and 3D targets with ground-truth information (\chref{opt}).
The videos are recorded under various lighting conditions, different motion
patterns and speeds with the help of a \textit{programmable robotic arm}.

\item
A novel and robust scheme to annotate the ground-truth poses
by leveraging the clear infrared images recorded by the
\textit{global-shutter} infrared camera with fast shutter speed from the \textit{Kinect V2} sensor,
which enables us to record sequence even under rapid motions (\chref{opt}).

\item
An efficient \textit{direct approach} of approximate pose estimation for planar objects
which is posed as a template matching problem (\chref{dpe}).
The proposed method performs robustly even when the
target images contain less textured surfaces or motion blurs.

\item
An accurate pose refinement strategy using a \textit{Lucas-Kanade dense alignment} scheme
(Chapter \ref{ch:dpe},\ref{ch:dodecapen}).
In this approach, the image changes with respect to the 6DoF pose,
which is parameterized as a 6D vector consisting of the 3D
\textit{rotation vector} and the 3D \textit{translation vector},
are approximated using the first-order Taylor series.
In addition, a \textit{backtracking line search} is performed
to ensure the convergence of Gauss-Newton iteration within the pose refinement technique.


\item
A practical method for planar object \textit{pose disambiguation},
which find all possible poses first and then the one with smallest \textit{appearance distance}
between the camera image and the target image
is considered as the estimated pose~\chref{dpe}.

\item
An accurate 6DoF pose tracking solution by leveraging
both the binary square fiducial marker toolkit
and the proposed pose refinement scheme~\chref{dodecapen}.
As each fiducial marker has many sharp edges and corners,
it is well-suited for providing a precise pose using dense alignment methods.
In addition, since there are large portions of the fiducial marker
that do not significantly contribute to the pose estimation procedure,
we can take advantage of this by selectively masking out flat regions
ahead of time on markers.
A significant acceleration of the algorithm can be achieved
without compromising tracking quality using this masking technique.

\item
A one-time \textit{model calibration} procedure using bundle adjustment
based on the proposed pose refinement algorithm~\chref{dodecapen}.
This technique can be employed not only for a dodecahedron
but also for any 3D model composed of planes.

\item
Extensive experimental evaluations 
of the proposed method as well as previous approaches
on both synthetic data and real data (Chapter \ref{ch:opt},\ref{ch:dpe},\ref{ch:dodecapen}).

\item
The implementation of an accurate pen-trajectory tracking solution,
which is comparable to state-of-the-art professional motion capture systems~\chref{dodecapen}.

\item
Demonstrations of the proposed accurate and easy-to-setup 6DoF stylus tracking system
for the application of drawing in 2D and 3D
as well as object manipulation in a virtual reality environment.

\end{itemize}

\section{Discussion and Future Work}
\label{sec:discussion_and_future_work}
Recently, object pose estimation has benefitted
from the advent of deep learning based approaches and the possibility
of using large datasets for training such methods,
and we believe this trend will continue in the future.
%
But because these deep learning based approaches generally cannot give pretty accurate results,
another pose refinement procedure is preferable to use.
Traditionally, the iterative closest point algorithm is commonly applied
to accomplish getting a more accurate pose.
However, since the depth image obtained by present sensors is still noisy
(as we have discussed in~\secref{obtaining_object_pose}),
the pose recovering results may not be entirely satisfactory.
In contrast, because the proposed pose refinement approach has been demonstrated
to improve the accuracy of the estimated pose significantly
with RGB images (which can be captured clearly by present sensors),
it is suitable for being augmented by any object pose estimation method
especially when the object is composed of planes.
But one should still be careful with the appearance consistency between
the targets in the camera image and its original representation
when using the dense alignment strategy.

There are many object pose estimation and tracking datasets 
which use fiducial markers to establish ground-truth poses.
Nonetheless, as we have presented in~\chref{dodecapen},
the marker corner alignment is insufficient for robust and accurate tracking.
Therefore, the pose annotation process in the previous benchmark dataset may not be reliable.
By employing the pose tracking solution proposed in~\chref{dodecapen},
it is conceivable to achieve more accurate and robust pose annotation results.
We also encourage everyone to utilize the dense pose refinement technique
proposed in~\secref{dense_pose_refinement} when using any fiducial marker toolkit
as it can phenomenally enhance the tracking quality with slight additional computational cost.

Pose estimation for planar objects can explicitly take advantage of depth images
as the estimated pose can, therefore, be unambiguous.
The candidate poses in the approximate pose estimation step presented in~\secref{ape_dpe}
can also be significantly reduced.
If there is no depth information, we can still use some temporal filtering strategy
to not only disambiguate the estimated pose but further improve the result accuracy as well.



